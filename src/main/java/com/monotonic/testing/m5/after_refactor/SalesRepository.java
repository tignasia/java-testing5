package com.monotonic.testing.m5.after_refactor;

import java.io.PrintStream;
import java.util.List;

public interface SalesRepository {

    List<Sale> loadSales();
     void setError(PrintStream error);

}
